import ...pnsc
using import utils.files
using import utils.utils

define c
    import-c "lib.c"
        """"#include <stdlib.h>
        list;

fn main ()
    let net state state2 =
        local pnsc.Net
        local pnsc.State
        local pnsc.State
    'load net "examples/example.pns"
    'load state net "examples/example.pnsi"
    'load state2 net "examples/example.pnsi"
    
    let MAX_COUNT = 64:usize;
    let names =
        alloca-array i8
            MAX_COUNT * net.transition_count

    let file =
        local File "examples/example.pnk" "r"
    for i in (range 0:usize net.transition_count)
        let name =
            names @ (i * MAX_COUNT)
        getline
            bitcast (allocaof (storage name)) (pointer (pointer i8 'mutable) 'mutable)
            local u64 MAX_COUNT
            storage& file
    delete file


    let iterator =
        local pnsc.Iterator
    
    loop (count forward) = 
        'init iterator state true
        true
    if (count == 0)
        print "Switch play directions!"
        repeat
            'init iterator state
                not forward
            not forward
    let transitions =
        alloca-array pnsc.Transition count

    print "Choose a transition:"
    for i in (range 0:u32 count)
        transitions @ i =
            'next iterator
        print
            (string-repr (i + 1)) .. ":"
            string.from-cstr
                bitcast (names @ ((transitions @ i . value) * MAX_COUNT)) rawstring
    let num =
        prompt ""
    let n =
        bitcast num rawstring
    let select =
        u32 (c.atoi num)
    if (select == 0)
        print "Switch play directions!"
        repeat
            'init iterator state
                not forward
            not forward
    if (select <= count)
        if forward
            'fire (load (transitions @ (select - 1:u32))) state
        else
            'unfire (load (transitions @ (select - 1:u32))) state
    else
        print "Number has to be a valid number from 1 to" count "or zero"
    repeat
        'init iterator state forward
        forward

    delete state
    delete state2
    delete net

main;
