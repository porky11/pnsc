define lib
    load-library "../pns/pns.so"
    import-c "lib.c"
        """"#include "pns.h"
        list "-I" "../pns/";

do
    let Net = lib.pns_Net
    let State = lib.pns_State
    let Iterator = lib.pns_Iterator
    let Transition = lib.pns_Transition

    set-type-symbol!& Net 'load lib.pns_Net_load
    #set-type-symbol!& Net '__init lib.pns_Net_load
    set-type-symbol!& Net '__delete lib.pns_Net_delete

    set-type-symbol!& State 'load lib.pns_State_load
    set-type-symbol!& State '__delete lib.pns_State_delete

    set-type-symbol!& Iterator 'init lib.pns_Iterator_init
    set-type-symbol!& Iterator 'next lib.pns_Iterator_next
    set-type-symbol!& Iterator 'next? lib.pns_Iterator_hasNext

    set-type-symbol! Transition 'fire lib.pns_Transition_fire
    set-type-symbol! Transition 'unfire lib.pns_Transition_unfire

    typefn& Iterator '__new (self ...)
        if (not (va-empty? ...))
            'init self ...

    locals;


